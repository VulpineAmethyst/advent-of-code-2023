#!/usr/bin/env python3
# Copyright 2023 Síle Ekaterin Liszka
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

from itertools import cycle
from math import lcm

insns = ''
data = []

with open('input.txt', mode='r') as f:
	data = f.readlines()

	insns = data[0][:-1]
	data = data[2:]

nodes = {}

for line in data:
	name, dest = line.split(' = ', maxsplit=1)
	left, right = dest[1:-2].split(', ', maxsplit=1)
	nodes[name] = (left, right)

move = {'L': 0, 'R': 1}

locs = [x for x in nodes.keys() if x.endswith('A')]
steps = {}
for loc in locs:
	orig = loc
	count = 0
	insn = cycle(insns)
	while not loc.endswith('Z'):
		dir = next(insn)
		loc = nodes[loc][move[dir]]
		count += 1

	steps[orig] = count

steppy = steps.values()

print(lcm(*steppy))
