#!/usr/bin/env python3
# Copyright 2023 Síle Ekaterin Liszka
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

total = 0
cards = {}
deck = 1
with open('input.txt', mode='r') as f:
	for line in f.readlines():
		line = line.removesuffix('\n')
		card, numbers = line.split(': ', maxsplit=1)
		_, card = card.split(' ', maxsplit=1)
		card = int(card)
		winning, hand = numbers.split(' | ', maxsplit=1)
		winning = winning.split()
		hand = hand.split()
		score = 0
		for num in hand:
			if num in winning:
				score += 1
		cards[card] = score
		deck += 1

def score(idx):
	global cards
	ret = cards[idx]
	if ret == 0:
		return ret
	x = ret + 1
	for i in range(1, x):
		ret += score(idx + i)
	return ret

count = deck - 1
for i in range(1, deck):
	count += score(i)

print('total cards is', count)
