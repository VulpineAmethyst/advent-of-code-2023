#!/usr/bin/env perl
# Copyright 2023 Síle Ekaterin Liszka
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

use strict;
use warnings;
use feature qw(:5.36);
use autodie;

open my $fh, '<', 'input.txt';

my %cap = (
	red   => 12,
	green => 13,
	blue  => 14
);

my $games = 0;

for my $line (<$fh>) {
	chomp $line;
	$line =~ s/^Game (\d+): //;
	my $id = $1;
	my @hands = split /; /, $line;
	my $possible = 1;
	my %minimums = (
		red => 0, blue => 0, green => 0
	);
	for my $hand (@hands) {
		my @colors = split /, /, $hand;
		my %cubes = ();
		for my $color (@colors) {
			my ($cubes, $col) = split / /, $color;
			$cubes{$col} = $cubes;
		}
		for my $color (qw(red blue green)) {
			next unless exists($cubes{$color});
			if ($cubes{$color} > $minimums{$color}) {
				$minimums{$color} = $cubes{$color};
			}
		}
		last unless $possible;
	}
	my $power = $minimums{red} * $minimums{green} * $minimums{blue};
	$games += $power;
}
say $games;
