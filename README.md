# Advent of Code 2023

This repository contains solutions to puzzles found in Advent of Code 2023.

# Usage

* Days 1-2, 6: Perl
* Days 4-5, 7: Python3

# License

The contents of this repository are published under the MIT license. See `LICENSE` for details.

