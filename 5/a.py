#!/usr/bin/env python3
# Copyright 2023 Síle Ekaterin Liszka
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

data = []
with open('input.txt', mode='r') as f:
	for line in f.readlines():
		data.append(line[:-1])

_, seeds = data[0].split(': ', maxsplit=1)
seeds = [int(seed) for seed in seeds.split(' ')]
data = data[2:]

direction = []
maps = {}
for line in data:
	if ':' in line:
		d, _ = line.split(' ', maxsplit=1)
		direction.append(d)
		maps[d] = []
		continue

	if not len(line):
		continue

	dest, src, rng = line.split(' ', maxsplit=2)
	mod = int(dest) - int(src)
	maps[d].append((int(dest), int(src), int(rng), mod))

seed_map = {}
for seed in seeds:
	t = seed
	m = {}
	for step in direction:
		for r in maps[step]:
			src, rng, mod = r[1:]
			if src <= t <= (src+rng):
				x = t + mod
				t = x
				m[step] = t
				break
		if step not in m.keys():
			m[step] = t
	seed_map[seed] = [str(m[step]) for step in direction]

lowest = int('9' * len(str(max(seeds))))
for seed in seeds:
	lowest = min(lowest, int(seed_map[seed][-1]))
	print(seed, '->', seed_map[seed][-1] + '; lowest is', lowest)
