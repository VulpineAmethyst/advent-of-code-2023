#!/usr/bin/env perl
# Copyright 2023 Síle Ekaterin Liszka
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

use strict;
use warnings;
use feature qw(:5.36);
use autodie;

use List::Util qw(reduce);

open my $fh, '<', 'input.txt';

my $data = '';
{
	local $/;
	$data = <$fh>;
	chomp $data;
}
my ($times, $distances) = split /\n/, $data, 2;

my @times = split / +/, $times;
shift @times;
my @distances = split / +/, $distances;
shift @distances;

sub calc_combos($time, $target) {
	my $ret = 0;
	for my $i (1 .. ($time - 1)) {
		my $travel = ($time - $i) * $i;
		$ret++ if ($travel > $target);
	}
	return $ret;
}

my @combos;
for my $race (0 .. $#times) {
	my $time = $times[$race];
	my $target = $distances[$race];
	push @combos, calc_combos($time, $target);
}

say join(' ', @combos);
say reduce { $a * $b } @combos;
